package com.getjavajob.training.siavrisi.email.service;

import com.getjavajob.training.siavrisi.email.model.Email;
import com.getjavajob.training.siavrisi.email.model.Message;
import com.getjavajob.training.siavrisi.email.repository.EmailRepository;
import com.getjavajob.training.siavrisi.email.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.sql.Date;
import java.util.Properties;

@Service
public class Consumer {

    private static final String FROM = "testingjmsconsumer@gmail.com";
    private static final String SUBJECT = "Some subject";

    private final MessageConsumer messageConsumer;
    private final EmailRepository emailRepository;
    private final MessageRepository messageRepository;

    @Autowired
    public Consumer(MessageConsumer consumer, EmailRepository emailRepository, MessageRepository queueRepository) {
        this.messageConsumer = consumer;
        this.emailRepository = emailRepository;
        this.messageRepository = queueRepository;
    }

    public void consume() throws JMSException, IOException, MessagingException {
        while (true) {
            javax.jms.Message message = messageConsumer.receive();
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                messageRepository.save(new Message(textMessage.getText()));
                String[] text = textMessage.getText().split("\n");
                Properties properties = new Properties();
                properties.load(Consumer.class.getClassLoader().getResourceAsStream("mail.properties"));
                javax.mail.Session mailSession = javax.mail.Session.getInstance(properties, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(properties.getProperty("mail.username"),
                                properties.getProperty("mail.password"));
                    }
                });
                javax.mail.Message mailMessage = new MimeMessage(mailSession);
                mailMessage.setFrom(new InternetAddress(FROM));
                mailMessage.setRecipient(javax.mail.Message.RecipientType.TO,
                        new InternetAddress(text[0]));
                mailMessage.setSubject(SUBJECT);
                mailMessage.setText(text[1]);
                Email email = new Email();
                email.setSender(FROM);
                email.setReceiver(text[0]);
                email.setSubject(SUBJECT);
                email.setText(text[1]);
                email.setDate(new Date(System.currentTimeMillis()));
                emailRepository.save(email);
                Transport.send(mailMessage);
            }
        }
    }

}

