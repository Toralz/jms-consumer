package com.getjavajob.training.siavrisi.email.repository;

import com.getjavajob.training.siavrisi.email.model.Email;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmailRepository extends MongoRepository<Email, String> {

}
