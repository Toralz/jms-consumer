package com.getjavajob.training.siavrisi.email;

import com.getjavajob.training.siavrisi.email.service.Consumer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.jms.JMSException;
import javax.mail.MessagingException;
import java.io.IOException;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private final Consumer consumer;

    public Application(Consumer consumer) {
        this.consumer = consumer;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws MessagingException, JMSException, IOException {
        consumer.consume();
    }

}

