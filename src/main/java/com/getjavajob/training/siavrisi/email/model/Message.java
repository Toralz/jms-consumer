package com.getjavajob.training.siavrisi.email.model;

import org.springframework.data.annotation.Id;

public class Message {

    @Id
    private String id;
    private String text;

    public Message(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
