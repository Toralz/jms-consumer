package com.getjavajob.training.siavrisi.email.repository;

import com.getjavajob.training.siavrisi.email.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageRepository extends MongoRepository<Message, String> {

}
